from jira import JIRA
import re
import datetime
import requests
import json
import csv
import yaml
from yaml import Loader


if __name__ == '__main__':

    stream = open('config.yaml', 'r')
    config = yaml.load(stream, Loader=Loader)

    clockify_request_headers = {'X-Api-Key': config['clockify']['token']}
    jira = JIRA(basic_auth=(config['jira']['username'], config['jira']['password']),
                server=config['jira']['url'],
                options={"verify": False})  # cuz on VPN

    work_workspace_id = [x for x in json.loads(requests.request('GET', url=config['clockify']['url'] + '/workspaces', headers=clockify_request_headers).content) if x['name'] == config['clockify']['workspace']][0]['id']
    user_id = json.loads(requests.request('GET', url=config['clockify']['url'] + '/user', headers=clockify_request_headers).content)['id']
    project_id = [x for x in json.loads(requests.request(
        'GET', url=config['clockify']['url'] + '/workspaces/' + work_workspace_id + '/projects', headers=clockify_request_headers)
                                        .content) if x['name'] == config['clockify']['project']][0]['id']

    new_url = config['clockify']['url'] + '/workspaces/' + work_workspace_id + '/user/' + user_id + '/time-entries'

    monday = (datetime.datetime.now() -
              datetime.timedelta(days=datetime.date.today().weekday())).replace(hour=0, minute=0, second=0)

    results = {}

    for day in range(0, 5):
        current_day = monday + datetime.timedelta(days=day)

        request_params = {'start': current_day.isoformat()[0:23]+'Z',
                          'end': (current_day + datetime.timedelta(days=1)).isoformat()[0:23]+'Z',
                          'project': project_id}

        task_name_list = [x['description'] for x in json.loads(
            requests.request('GET', url=new_url, headers=clockify_request_headers,
                             params=request_params).content) if config['jira']['projectPrefix'] in x['description']]
        distinct_tasks = set()

        distinct_tasks.update([re.findall(config['jira']['projectPrefix']+'-\d{' + str(config['jira']['ticketNumberLength']) + '}', x)[0] for x in task_name_list])

        result_list = list()

        for ticket_name in distinct_tasks:
            issue = jira.issue(ticket_name)
            issue_key_and_description = issue.key + ' - ' + issue.fields.summary
            result_list.append(issue_key_and_description)

        result_for_day = '; '.join(result_list)
        results[current_day.strftime('%d.%m.%Y')] = result_for_day

    with open('ln.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect='excel', delimiter=',')
        for result_date in results.keys():
            writer.writerow([result_date, config['csv']['lnCode'], results[result_date], config['csv']['fullName'], config['csv']['hours']])
